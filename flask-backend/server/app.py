import os

from flask import Flask
from flask.json import jsonify

from models.Contextual import Contextual
from models import Collaborative
from models import Conversion
from os import environ

app = Flask(__name__, instance_relative_config=True)


cont = Contextual()

@app.route('/recommendation/<movie>')
def get_recommendation(movie):
    return jsonify({'results': cont.get_rec_json(cont.get_recommendations(movie))})

@app.route('/top')
def get_top():
    return jsonify({'results': cont.get_rec_json(cont.get_first_movies(10))})

@app.route('/for_me/<int:userId>')
def get_for_me(userId):
    pr = Collaborative.predict(userId)
    print(pr)
    uids = []
    for (mid, rating) in pr:
        imdb_id = Conversion.getImdbId(mid)
        if imdb_id != -1:
            uids.append(int(imdb_id))
    return jsonify({'results': cont.get_rec_json_imdb(uids)})

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    return response


if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)